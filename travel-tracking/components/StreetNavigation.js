import { useEffect, useRef, useState, useContext } from 'react'
import { Row, Col, Card, Button, Alert } from 'react-bootstrap'
import mapboxgl from 'mapbox-gl'
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY
import MapboxDirections from '@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions'
import Router from 'next/router'

import UserContext from '../UserContext'

const StreetNavigation = () => {
	
	const { user } = useContext(UserContext)
	const mapContainerRef = useRef(null)

	const [distance, setDistance] = useState(0)
	const [duration, setDuration] = useState(0)
	const [originLong, setOriginLong] = useState(0)
	const [originLat, setOriginLat] = useState(0)
	const [destinationLong, setDestinationLong] = useState(0)
	const [destinationLat, setDestinationLat] = useState(0)
	const [isActive, setIsActive] = useState(false)

	useEffect(()=>{

		const map = new mapboxgl.Map({
			container: mapContainerRef.current,
			style: 'mapbox://styles/mapbox/streets-v11',
			center: [121.04382, 14.63289],
			zoom: 12
		})

		//Instantiate mapbox directions control overlay
		const directions = new MapboxDirections({
			accessToken: mapboxgl.accessToken,
			unit: 'metric',
			profile: 'mapbox/driving'
		})

		// add navigation control
		map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')

		// add Mapbox Directions Control
		map.addControl(directions, 'top-left')

		directions.on("route", e => {
			console.log(e)

			if(typeof e.route !== "undefined"){
				console.log(e.route)
			}
		})
		
		return () => map.remove()
	}, [])

	return (
		<Row>
			<Col xs={12} md={8}>
				<div className="mapContainer" ref={mapContainerRef}/>
			</Col>
			<Col xs={12} md={4}>
				{user.id === null
				?
				<Alert variant="info">You must be logged in to record your travels</Alert>
				:
				<Card>
					<Card.Body>
						<Card.Title>
							Record Route
						</Card.Title>
						<Card.Title>
							Origin Longitude: {originLong}
						</Card.Title>
						<Card.Title>
							Origin Latitude: {originLat}
						</Card.Title>
						<Card.Title>
							Destination Longitude: {destinationLong}
						</Card.Title>
						<Card.Title>
							estination Latitude: {destinationLat}
						</Card.Title>
						<Card.Title>
							Total Distance: {Math.round(distance)} meters
						</Card.Title>
						<Card.Title>
							Total Duration: {Math.round(duration/60)} minutes
						</Card.Title>
						{isActive ===true
						?
						<Button variant="success" onClick={recordTravel}>Record</Button>
						:
						<Button variant="secondary" disabled>Record</Button>
						}
					</Card.Body>
				</Card>
				}
			</Col>
		</Row>
	)
}

export default StreetNavigation